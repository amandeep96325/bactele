import React, { Component } from "react";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import PageHeader from '../../../components/utility/pageHeader';
import TableWrapper from '../../../containers/Tables/antTables/antTable.style';
import Button from '../../../components/uielements/button';
import { Row } from "antd";

const dataSource = [{
  key: '1',
  id: 1,
  name: 'Advent Health',
  action: 'JOIN ROOM'
}, {
  key: '2',
  id: 2,
  name: 'Florida Medical Clinic',
  action: 'JOIN ROOM'
}];

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Room Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
  }
];

export default class extends Component {

  render() {

    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'
    };
    const gutter = 16;

    return (
      <LayoutContentWrapper>
        <PageHeader> VIDEO ROOM </PageHeader>
        <LayoutContent>

          <TableWrapper
            pagination={true}
            columns={columns}
            dataSource={dataSource}
            className="isoSimpleTable"
          />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
