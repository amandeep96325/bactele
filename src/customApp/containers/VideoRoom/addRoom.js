import React, { Component } from "react";
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import Button from '../../../components/uielements/button';
import Select, { SelectOption } from '../../../components/uielements/select';
import { connect } from 'react-redux';
import addAction from '../../../redux/videoRoom/actions';
import Moment from 'react-moment';
import 'moment-timezone';

const {addRooms} = addAction;
const Option = SelectOption;
const FormItem = Form.Item;
class AddRoom extends Component {
  state = {
    redirectToReferrer: false,
    gender : '',
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  handleAdd = () => {
    const { signup } = this.props;
    const userInfo = {
      name: document.getElementById('inputName').value || ''
    };
    this.props.addRooms(userInfo);
  }

  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;

    return (
      <LayoutWrapper>
        <PageHeader> Add Video Room </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box>
              <ContentHolder>
                <Form>
                  <FormItem className="form-d">
                    <Row style={rowStyle} gutter={gutter} justify="start">
                        <p>Name</p>
                        <Input id="inputName" placeholder="Enter video room name" />
                    </Row>
                  </FormItem>
                </Form>
                <Button style={buttonStyle} type="primary" onClick={this.handleAdd}>Submit</Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>

      </LayoutWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
  }),
  { addRooms }
)(AddRoom);
