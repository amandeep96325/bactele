import React, { Component } from "react";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import PageHeader from '../../../components/utility/pageHeader';
import TableWrapper from '../../../containers/Tables/antTables/antTable.style';
import Button from '../../../components/uielements/button';
import { Row } from "antd";

const dataSource = [{
  key: '1',
  id: 1,
  name: 'Advent Health',
  address: '10 Downing Street',
  render: () => (
    <a href="/">Delete</a>
  )
}, {
  key: '2',
  id: 2,
  name: 'Florida Medical Clinic',
  address: '10 Downing Street',
  render: () => (
    <a href="/">Delete</a>
  )
}];

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Group Name',
    dataIndex: 'name',
    key: 'name',
  }, 
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
  },
  {
    title: 'Publish',
    dataIndex: 'publish',
    key: 'publish',
  },
  {
    title: 'Delete',
    dataIndex: 'delete',
    key: 'delete',
  }
];

export default class extends Component {
  
  render() {

    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'
    };
    const gutter = 16;

    return (
      <LayoutContentWrapper>
        <PageHeader> Practice Groups </PageHeader>
        <LayoutContent>
          <Row style={firstRowStyle}><Button style={buttonStyle} type="primary">Add New Group</Button></Row>
          
          <TableWrapper
            pagination={true}
            columns={columns}
            dataSource={dataSource}
            className="isoSimpleTable"
          />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
