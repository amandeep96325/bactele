import React, { Component } from "react";
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import Button from '../../../components/uielements/button';
import Radio, { RadioGroup } from '../../../components/uielements/radio';

const FormItem = Form.Item;

export default class extends Component {
  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;
  
    return (
      <LayoutWrapper>
        <PageHeader> Add Group </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box>
              <ContentHolder>
                <Form>
                  <FormItem >
                    <p>Group name</p>
                    <Input placeholder="Enter group name" />
                  </FormItem>
                  <FormItem >
                    <p>Group status</p>
                    <RadioGroup>
                      <Radio value={1}>Active</Radio>
                      <Radio value={2}>Inactive</Radio>
                    </RadioGroup>
                  </FormItem>
                </Form>
                <Button style={buttonStyle} type="primary">Submit</Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}