import React, { Component } from "react";
import { connect } from 'react-redux';
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import { BoxTitle, BoxSubTitle } from '../../../components/utility/boxTitle.style';
import Button from '../../../components/uielements/button';
import PageHeader from '../../../components/utility/pageHeader';
import loginAction from '../../../redux/settings/actions';
import { Link } from 'react-router-dom';
const { viewProfile } =loginAction;


class Profile extends Component {
  constructor (props) {
       super(props)
   }

// debugger;
  state = {
    redirectToReferrer: false,
  };
  componentWillMount(){
    const users = this.props.viewProfile();
    // debugger;
  }


  render() {
    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;
    const {users} = this.props;
    console.log(users.data)
    // debugger;
    return (
      <LayoutContentWrapper>
        <PageHeader> Profile </PageHeader>
        <Row style={firstRowStyle}><a style={buttonStyle} class="ant-btn ant-btn-primary" type="primary" href="/dashboard/edit_profile">Edit Profile</a></Row>
        <LayoutContent>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
          <Box>
            <ContentHolder>
              <Row style={rowStyle} gutter={gutter} justify="start">
                <Col md={24} sm={24} xs={24} style={colStyle}>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>NAME:</b></span>
                      <span>{users.data.first_name} {users.data.last_name}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>EMAIL:</b></span>
                      <span>{users.data.email}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>CELL NUMBER:</b></span>
                      <span>{users.data.cell_no}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>ROLE:</b></span>
                      <span>{users.data.role}</span>
                    </div>
                  </div>
                </Col>
              </Row>
              </ContentHolder>
              </Box>
          </Col>
        </Row>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    users: state.Settings
  }),
  { viewProfile }
)(Profile);
