import React, { Component } from "react";
import { connect } from 'react-redux';
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import { BoxTitle, BoxSubTitle } from '../../../components/utility/boxTitle.style';
import Button from '../../../components/uielements/button';
import PageHeader from '../../../components/utility/pageHeader';
import loginAction from '../../../redux/settings/actions';
import { Link } from 'react-router-dom';
const { changePassword } =loginAction;
const FormItem = Form.Item;


class Password extends Component {
  state = {
    redirectToReferrer: false,
  };
  constructor (props) {
       super(props)
   }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  handleedit = () => {
    const { signup } = this.props;
    const userInfo = {
      old_password: document.getElementById('oldpassword').value || '',
      new_password: document.getElementById('newpassword').value || '',
    };
    this.props.changePassword(userInfo);
  }

  render() {
    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;
    const {users} = this.props;
    console.log(this.state)
    // debugger;
    return (
      <LayoutWrapper>
        <PageHeader> Change Password </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box>
              <ContentHolder>
                <Form>
                  <FormItem className="form-d">
                    <Row style={rowStyle} gutter={gutter} justify="start">
                        <p>Old password</p>
                        <Input id="oldpassword" placeholder="Enter your Old password" name="first_name"/>

                        <p>New password</p>
                        <Input id="newpassword" placeholder="Enter New Password"  name="last_name"/>

                    </Row>
                  </FormItem>
                </Form>
                <Button style={buttonStyle} type="primary" onClick={this.handleedit}>Submit</Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    users: state.Settings
  }),
  { changePassword }
)(Password);
