import React, { Component } from "react";
import { connect } from 'react-redux';
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import { BoxTitle, BoxSubTitle } from '../../../components/utility/boxTitle.style';
import Button from '../../../components/uielements/button';
import PageHeader from '../../../components/utility/pageHeader';
import loginAction from '../../../redux/settings/actions';
import { Link } from 'react-router-dom';
const { viewProfile , editProfile } =loginAction;
const FormItem = Form.Item;


class Profile extends Component {
  state = {
    redirectToReferrer: false,
    first_name: '',
    last_name:   '',
    email: '',
    cell_no: '',
  };
  constructor (props) {
       super(props)
   }
   handleChange = (event) =>  {
     this.setState({[event.target.name]: event.target.value})
   }
  componentWillMount(){
    const users = this.props.viewProfile();
    // debugger;
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps.users.data);
    this.setState(nextProps.users.data);
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  handleedit = () => {
    const { signup } = this.props;
    const userInfo = {
      first_name: document.getElementById('inputfirstName').value || '',
      last_name: document.getElementById('inputLastName').value || '',
      email: document.getElementById('inputEmail').value || '',
      cell_no: document.getElementById('Mobile').value || ''
    };
    this.props.editProfile(userInfo);
  }

  render() {
    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;
    const {users} = this.props;
    console.log(this.state)
    // debugger;
    return (
      <LayoutWrapper>
        <PageHeader> Edit Patient </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box>
              <ContentHolder>
                <Form>
                  <FormItem className="form-d">
                    <Row style={rowStyle} gutter={gutter} justify="start">
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>First name</p>
                        <Input id="inputfirstName" placeholder="Enter patient first name" name="first_name" value={this.state.first_name} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Last name</p>
                        <Input id="inputLastName" placeholder="Enter patient first name"  name="last_name" value={this.state.last_name} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Email</p>
                        <Input id="inputEmail" type="email" placeholder="Enter patient email" name="email" value={this.state.email} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Mobile Telephone</p>
                        <Input id="Mobile" placeholder="Enter patient mobile telephone" name="cell_no" style={{ width: '100%' }} value={this.state.cell_no} onChange={this.handleChange}/>
                      </Col>
                    </Row>
                  </FormItem>
                </Form>
                <Button style={buttonStyle} type="primary" onClick={this.handleedit}>Submit</Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    users: state.Settings
  }),
  { viewProfile , editProfile }
)(Profile);
