import React, { Component } from "react";
import { connect } from 'react-redux';
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import PageHeader from '../../../components/utility/pageHeader';
import TableWrapper from '../../../containers/Tables/antTables/antTable.style';
import Button from '../../../components/uielements/button';
import loginAction from '../../../redux/users/actions';
import { Row } from "antd";
import { Link } from 'react-router-dom';
const {viewUsers , deleteUser} =loginAction;


class SignUp extends Component {
  constructor (props) {
       super(props)
   }


  state = {
    redirectToReferrer: false,
    columns : [
      {
        title: 'ID',
        dataIndex: 'user_id',
        key: 'id',
      },
      {
        title: 'First Name',
        dataIndex: 'first_name',
        key: 'first_name',
      },
      {
        title: 'Last Name',
        dataIndex: 'last_name',
        key: 'last_name',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Role',
        dataIndex: 'role',
        key: 'role',
      },
      {
        title: 'Action',
        render: (text, record) => (
          <span>
            <Link to={`/dashboard/detail_patients/${record.user_id}`}>View</Link>
            <span className="ant-divider" />
            <a href={`/dashboard/edit_patients/${record.user_id}`}>Edit</a>
            <span className="ant-divider" />
            <a
            onClick={() => this.delete(record)}
            >Delete</a>
          </span>
        ),
        key: 'delete',
      }
    ]
  };
  componentWillMount(){
    const users = this.props.viewUsers();
  }


  delete (record) {
      this.props.deleteUser(record.user_id);
  }



  render() {


    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'
    };
    const gutter = 16;
    const {users} = this.props
    // debugger;
    return (
      <LayoutContentWrapper>
        <PageHeader> Users </PageHeader>
        <LayoutContent>
          <Row style={firstRowStyle}><a style={buttonStyle} class="ant-btn ant-btn-primary" type="primary" href="/dashboard/add_patient">Add Patient</a></Row>

          <TableWrapper
            pagination={true}
            columns={this.state.columns}
            dataSource={users.data}
            className="isoSimpleTable"
          />

        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    users: state.Users
  }),
  { viewUsers, deleteUser }
)(SignUp);
