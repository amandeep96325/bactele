import React, { Component } from 'react';
import { connect } from 'react-redux';
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import actions from '../../redux/users/actions';

const { testingGet } = actions;

class Testing extends Component {

    onLoad = () => {
        console.log("I'm in load");
        this.props.testingGet();
    }

    componentWillMount() {
        this.onLoad();
    }

    render() {

        const { Testing } = this.props;

        return(
            <LayoutContentWrapper style={{ height: "100vh" }}>
                <LayoutContent>
                    <h2>Testing</h2>
                    {Testing.result.map(item => {
                        const name = item.name;
                        return(<p key={item.id}>{item.name}</p>);
                    })}
                </LayoutContent>
            </LayoutContentWrapper>
        );
    }

}

function mapStateToProps(state){
    return { Testing: state.testing }
}

export default connect(
    mapStateToProps,
    { testingGet }
)(Testing);