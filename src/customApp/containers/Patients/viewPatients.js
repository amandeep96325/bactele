import React, { Component } from "react";
import { connect } from 'react-redux';
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import LayoutContent from "../../../components/utility/layoutContent";
import PageHeader from '../../../components/utility/pageHeader';
import TableWrapper from '../../../containers/Tables/antTables/antTable.style';
import Button from '../../../components/uielements/button';
import loginAction from '../../../redux/patients/actions';
import { Row } from "antd";
import { Link } from 'react-router-dom';
const {viewPatients , deletePatient} =loginAction;


class SignUp extends Component {
  constructor (props) {
       super(props)
   }


  state = {
    redirectToReferrer: false,
    columns : [
      {
        title: 'ID',
        dataIndex: '_id',
        key: 'id',
      },
      {
        title: 'First Name',
        dataIndex: 'first_name',
        key: 'first_name',
      },
      {
        title: 'Last Name',
        dataIndex: 'last_name',
        key: 'last_name',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Cell Number',
        dataIndex: 'cell_no',
        key: 'cell_no',
      },
      {
        title: 'Action',
        render: (text, record) => (
          <span>
            <Link to={`/dashboard/detail_patients/${record._id}`}>View</Link>
            <span className="ant-divider" />
            <a href={`/dashboard/edit_patients/${record._id}`}>Edit</a>
            <span className="ant-divider" />
            <a
            onClick={() => this.delete(record)}
            >Delete</a>
          </span>
        ),
        key: 'delete',
      }
    ]
  };
  componentWillMount(){
    this.props.viewPatients();
  }


  delete (record) {
      this.props.deletePatient(record._id);
  }

  handleLogin = (token = false) => {
       if (token) {
        viewPatients({token});
      } else {
        viewPatients();
      }
    };




  render() {


    const firstRowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'flex-end'
    };

    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'
    };
    const gutter = 16;
    const {patients} = this.props
    return (
      <LayoutContentWrapper>
        <PageHeader> Patients </PageHeader>
        <LayoutContent>
          <Row style={firstRowStyle}><a style={buttonStyle} class="ant-btn ant-btn-primary" type="primary" href="/dashboard/add_patient">Add Patient</a></Row>

          <TableWrapper
            pagination={true}
            columns={this.state.columns}
            dataSource={patients.data}
            className="isoSimpleTable"
          />

        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    patients: state.Patients
  }),
  { viewPatients, deletePatient }
)(SignUp);
