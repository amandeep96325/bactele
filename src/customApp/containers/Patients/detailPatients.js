import React, { Component } from "react";
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import { BoxTitle, BoxSubTitle } from '../../../components/utility/boxTitle.style';
import PageHeader from '../../../components/utility/pageHeader';
import Button from '../../../components/uielements/button';
import Select, { SelectOption } from '../../../components/uielements/select';
import { connect } from 'react-redux';
import addAction from '../../../redux/patients/actions';
import Moment from 'react-moment';
import 'moment-timezone';
import './style.css'

const {detailPatients} = addAction;
const Option = SelectOption;
const FormItem = Form.Item;
class ViewPatients extends Component {
  state = {
    redirectToReferrer: false,
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  componentWillMount(){
    var getData= this.props.detailPatients(this.props.match.params.id);
  }
  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;
    const patients = this.props.patient.data;
    return (
      <LayoutWrapper>
        <PageHeader>Patient Profile </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
          <Box>
            <ContentHolder>
              <Row style={rowStyle} gutter={gutter} justify="start">
                <Col md={24} sm={24} xs={24} style={colStyle}>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>NAME:</b></span>
                      <span>{patients.first_name} {patients.middle_name} {patients.last_name}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>EMAIL:</b></span>
                      <span>{patients.email}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>CELL NUMBER:</b></span>
                      <span>{patients.cell_no}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>GENDER:</b></span>
                      <span>{patients.gender || '-'}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>PREFFERED LANGUAGE:</b></span>
                      <span>{patients.preferred_lang || '-'}</span>
                    </div>
                  </div>
                </Col>
                <Col md={24} sm={24} xs={24} style={colStyle}>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>PREFFERED LANGUAGE:</b></span>
                      <span>{patients.preferred_lang || '-'}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>PREFFERED PHONE NUMBER:</b></span>
                      <span>{patients.preferred_phone_no || '-'}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>PRIMARY INSURANCE:</b></span>
                      <span>{patients.primary_insurance || '-'}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>SECONDARY INSURANCE:</b></span>
                      <span>{patients.secondary_insurance || '-'}</span>
                    </div>
                  </div>
                  <div className="isoSignInForm">
                    <div className="isoInputWrapper dlex">
                      <span><b>BENEFICIARY CODE:</b></span>
                      <span>{patients.beneficiary_code || '-'}</span>
                    </div>
                  </div>
                </Col>
              </Row>
              </ContentHolder>
              </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    patient: state.Patients
  }),
  { detailPatients }
)(ViewPatients);
