import React, { Component } from "react";
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import { BoxTitle, BoxSubTitle } from '../../../components/utility/boxTitle.style';
import PageHeader from '../../../components/utility/pageHeader';
import Button from '../../../components/uielements/button';
import Select, { SelectOption } from '../../../components/uielements/select';
import { connect } from 'react-redux';
import addAction from '../../../redux/patients/actions';
import Moment from 'react-moment';
import 'moment-timezone';

const {detailPatients, editPatients} = addAction;
const Option = SelectOption;
const FormItem = Form.Item;
class EditPatients extends Component {
  state = {
    redirectToReferrer: false,
    first_name: '',
    last_name:   '',
    middle_name:  '',
    email: '',
    dob: '',
    gender: '',
    preferred_lang: '',
    preferred_phone_no: '',
    cell_no: '',
  };



  constructor(props = props.patient) {
    super(props);
  }

  handleChange = (event) =>  {
    this.setState({[event.target.name]: event.target.value})
  }
  componentWillMount(){
    var getData= this.props.detailPatients(this.props.match.params.id);
  }
componentWillReceiveProps(nextProps) {
  this.setState(nextProps.patient.data);
  if (
    this.props.isLoggedIn !== nextProps.isLoggedIn &&
    nextProps.isLoggedIn === true
  ) {
    this.setState({ redirectToReferrer: true });
  }
}


  handleedit = () => {
    const { signup } = this.props;
    const userInfo = {
      first_name: document.getElementById('inputfirstName').value || '',
      last_name: document.getElementById('inputLastName').value || '',
      middle_name: document.getElementById('inputMiddleName').value || '',
      email: document.getElementById('inputEmail').value || '',
      dob: document.getElementById('inputdob').value,
      gender: document.getElementsByClassName('ant-select-selection-selected-value').title || '',
      preferred_lang: document.getElementById('prefLang').value || '',
      preferred_phone_no: document.getElementById('Telephone').value || '',
      cell_no: document.getElementById('Mobile').value || '',
      patient_id: 1,
      patientid: this.props.match.params.id
    };
    this.props.editPatients(userInfo);
  }
  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;
    const patients = this.props.patient.data;
    console.log(this.state)
    return (
      <LayoutWrapper>
        <PageHeader> Edit Patient </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box>
              <ContentHolder>
                <Form>
                  <FormItem className="form-d">
                    <Row style={rowStyle} gutter={gutter} justify="start">
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>First name</p>
                        <Input id="inputfirstName" placeholder="Enter patient first name" name="first_name" value={this.state.first_name} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Middle name</p>
                        <Input id="inputMiddleName" placeholder="Enter patient middle name"  name="middle_name" value={this.state.middle_name} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Last name</p>
                        <Input id="inputLastName" placeholder="Enter patient first name"  name="last_name" value={this.state.last_name} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Patient DOB</p>
                        <Input id="inputdob" type="date" placeholder="mm/dd/yyyy" style={{ width: '100%' }} name="dob" onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Email</p>
                        <Input id="inputEmail" type="email" placeholder="Enter patient email" name="email" value={this.state.email} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Gender</p>
                        <Select id="gender" style={{ width: '100%' }} name="gender"  >
                            <Option value="advent">Male</Option>
                            <Option value="brandon">Female</Option>
                        </Select>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Preferred Language</p>
                        <Input id="prefLang" placeholder="Enter patient preferred language" name="preferred_lang" value={this.state.preferred_lang}  onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Preferred Telephone</p>
                        <Input id="Telephone" placeholder="Enter patient preferred telephone" name="preferred_phone_no" style={{ width: '100%' }} value={this.state.preferred_phone_no} onChange={this.handleChange}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Mobile Telephone</p>
                        <Input id="Mobile" placeholder="Enter patient mobile telephone" name="cell_no" style={{ width: '100%' }} value={this.state.cell_no} onChange={this.handleChange}/>
                      </Col>
                    </Row>
                  </FormItem>
                </Form>
                <Button style={buttonStyle} type="primary" onClick={this.handleedit}>Submit</Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
      // <LayoutWrapper>
      //   <PageHeader>Patient Profile </PageHeader>
      //   <Row style={rowStyle} gutter={gutter} justify="start">
      //     <Col md={12} sm={12} xs={24} style={colStyle}>
      //     <Box>
      //       <ContentHolder>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px',
      //               }}>NAME:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}> {patients.middle_name} {patients.last_name}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>EMAIL:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.email}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>CELL NUMBER:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.cell_no}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>GENDER:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.gender || '-'}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>PREFFERED LANGUAGE:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.preferred_lang || '-'}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>PREFFERED LANGUAGE:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.preferred_lang || '-'}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>PREFFERED PHONE NUMBER:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.preferred_phone_no || '-'}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>PRIMARY INSURANCE:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.primary_insurance || '-'}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>SECONDARY INSURANCE:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.secondary_insurance || '-'}</span>
      //             </div>
      //           </div>
      //         <div className="isoSignInForm">
      //             <div className="isoInputWrapper">
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>BENEFICIARY CODE:</span>
      //               <span style={{
      //                 alignItems: 'center',
      //                 justifyContent: 'center',
      //                 width: '50%',
      //                 padding: '100px'
      //               }}>{patients.beneficiary_code || '-'}</span>
      //             </div>
      //           </div>
      //         </ContentHolder>
      //         </Box>
      //     </Col>
      //   </Row>
      // </LayoutWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
    patient: state.Patients
  }),
  { detailPatients, editPatients }
)(EditPatients);
