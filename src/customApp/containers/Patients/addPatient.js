import React, { Component } from "react";
import { Col, Row } from 'antd';
import Input from '../../../components/uielements/input';
import Form from '../../../components/uielements/form';
import Box from '../../../components/utility/box';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import Button from '../../../components/uielements/button';
import Select, { SelectOption } from '../../../components/uielements/select';
import { connect } from 'react-redux';
import addAction from '../../../redux/patients/actions';
import Moment from 'react-moment';
import 'moment-timezone';

const {addPatients} = addAction;
const Option = SelectOption;
const FormItem = Form.Item;
class AddPatients extends Component {
  state = {
    redirectToReferrer: false,
    gender : '',
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  handleChange = (e) => {
this.setState({gender: e});

}
  handleAdd = () => {
    const { signup } = this.props;
    console.log(signup);
    const dateToFormat = document.getElementById('inputdob').value;
    const userInfo = {
      first_name: document.getElementById('inputfirstName').value || '',
      last_name: document.getElementById('inputLastName').value || '',
      middle_name: document.getElementById('inputMiddleName').value || '',
      email: document.getElementById('inputEmail').value || '',
      // dob: render() { return ( <Moment>{dateToFormat}</Moment> ); },
      gender: document.getElementById('gender').value || '',
      preferred_lang: document.getElementById('prefLang').value || '',
      preferred_phone_no: document.getElementById('Telephone').value || '',
      cell_no: document.getElementById('Mobile').value || '',
      patient_id: 1
    };
    console.log(userInfo);
    debugger;
    this.props.addPatients(userInfo);
  }

  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const buttonStyle = {
      float: 'right',
      marginBottom: '20px'

    };
    const gutter = 16;

    return (
      <LayoutWrapper>
        <PageHeader> Add Patient </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Box>
              <ContentHolder>
                <Form>
                  <FormItem className="form-d">
                    <Row style={rowStyle} gutter={gutter} justify="start">
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>First name</p>
                        <Input id="inputfirstName" placeholder="Enter patient first name" />
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Middle name</p>
                        <Input id="inputMiddleName" placeholder="Enter patient middle name" />
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Last name</p>
                        <Input id="inputLastName" placeholder="Enter patient first name" />
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Patient DOB</p>
                        <Input id="inputdob" type="date" placeholder="mm/dd/yyyy" style={{ width: '100%' }}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Email</p>
                        <Input id="inputEmail" type="email" placeholder="Enter patient email"/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Gender</p>
                        <Select id="gender" value={this.state.gender} onChange={this.handleChange} style={{ width: '100%' }}>
                            <Option value="Male">Male</Option>
                            <Option value="Female">Female</Option>
                        </Select>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Preferred Language</p>
                        <Input id="prefLang" placeholder="Enter patient preferred language" />
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Preferred Telephone</p>
                        <Input id="Telephone" placeholder="Enter patient preferred telephone" style={{ width: '100%' }}/>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <p>Mobile Telephone</p>
                        <Input id="Mobile" placeholder="Enter patient mobile telephone" style={{ width: '100%' }}/>
                      </Col>
                    </Row>
                  </FormItem>
                </Form>
                <Button style={buttonStyle} type="primary" onClick={this.handleAdd}>Submit</Button>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
  }),
  { addPatients }
)(AddPatients);
