import asyncComponent from '../helpers/AsyncFunc';

const routes = [
  // {
  //   path: 'githubSearch',
  //   component: asyncComponent(() => import('./containers/GithubSearch'))
  // },
  {
    path: 'add_patient',
    component: asyncComponent(() => import('./containers/Patients/addPatient'))
  },
  {
    path: 'view_patients',
    component: asyncComponent(() => import('./containers/Patients/viewPatients'))
  },
  {
    path: 'detail_patients/:id',
    component: asyncComponent(() => import('./containers/Patients/detailPatients'))
  },
  {
    path: 'edit_patients/:id',
    component: asyncComponent(() => import('./containers/Patients/editPatients'))
  },
  {
    path: 'view_groups',
    component: asyncComponent(() => import('./containers/PracticeGroups/viewGroups'))
  },
  {
    path: 'add_group',
    component: asyncComponent(() => import('./containers/PracticeGroups/addGroup'))
  },
  {
    path: 'view_users',
    component: asyncComponent(() => import('./containers/Users/viewUsers'))
  },
  {
    path: 'add_user',
    component: asyncComponent(() => import('./containers/Users/addUser'))
  },
  {
    path: 'testing',
    component: asyncComponent(() => import('./containers/Users/testing'))
  },
  {
    path: 'profile',
    component: asyncComponent(() => import('./containers/Settings/profile'))
  },
  {
    path: 'edit_profile',
    component: asyncComponent(() => import('./containers/Settings/editProfile'))
  },
  {
    path: 'change_password',
    component: asyncComponent(() => import('./containers/Settings/changePassword'))
  },
  {
    path: 'add_room',
    component: asyncComponent(() => import('./containers/VideoRoom/addRoom'))
  },
  {
      path: 'join_room/:token',
      component: asyncComponent(() => import('./containers/VideoRoom/joinRoom'))
    },
    {
        path: 'view_room',
        component: asyncComponent(() => import('./containers/VideoRoom/viewRooms'))
      },
];
export default routes;
