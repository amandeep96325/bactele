import githubSearch from './githubSearch/reducers';
import testing from './users/reducers';

export default { githubSearch, testing };
