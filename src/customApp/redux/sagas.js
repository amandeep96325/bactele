import { all } from 'redux-saga/effects';
import githubSearchSagas from './githubSearch/sagas';
import testingSagas from './users/sagas';

export default function* devSaga() {
  yield all([githubSearchSagas(), testingSagas()]);
}
