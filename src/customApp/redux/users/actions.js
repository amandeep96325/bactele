const actions = {
    TESTING_GET: "TESTING_GET",
    TESTING_GET_SUCCESS: "TESTING_GET_SUCCESS",
    TESTING_GET_ERROR: "TESTING_GET_ERROR",
    testingGet: () => ({
        type: actions.TESTING_GET
    }),
    testingGetSuccess: (result) => ({
        type: actions.TESTING_GET_SUCCESS,
        result
    }),
    testingGetError: () => ({
        type: actions.TESTING_GET_ERROR
    })
};
export default actions;