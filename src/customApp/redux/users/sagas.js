import { all, takeEvery, put, call } from 'redux-saga/effects';
import actions from './actions';

const onTestingGetRequest = async () => 
    await fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(res => res)
    .catch(error => error)

function* testingGetRequest() {

    try {
        const testingResults = yield call(onTestingGetRequest);
        console.log("I'm in testingGetRequest saga");
        if (Object.entries(testingResults).length !== 0) {
            console.log(JSON.stringify(testingResults));
            yield put(
                actions.testingGetSuccess(
                    testingResults
                )
            );
        } else {
            yield put(actions.testingGetSuccess());
        }

    } catch(error) {
        console.log(JSON.stringify(error));
    }
}

function* testingGetSuccess(result) {
    console.log("I'm in testingGetSuccess saga");
    if (result.result !== undefined) {
        const name = result.result[0].name;
        console.log(name);
        // if (!window.localStorage.getItem(name)) {
        //     window.localStorage.setItem('client_name', name);
        // }
    }
}

function* testingGetError(error) {
    console.log("I'm in testingGetError saga");
}

export default function* rootSaga() {
    yield all([takeEvery(actions.TESTING_GET, testingGetRequest), takeEvery(actions.TESTING_GET_SUCCESS, testingGetSuccess), takeEvery(actions.TESTING_GET_ERROR, testingGetError)])
}