import actions from './actions';

const initState = {
    result: []
};

export default function reducer(state=initState, action) {
    switch(action.type) {
        case actions.TESTING_GET:
            return { 
                ...state
            };
        case actions.TESTING_GET_SUCCESS:
            return {
                ...state,
                result: action.result
            };
        default:
            return state;
    }

}