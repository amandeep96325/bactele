const actions = {
  CHECK_AUTHORIZATION: 'CHECK_AUTHORIZATION',
  REGISTER_CLIENT: 'REGISTER_CLIENT',
  REGISTER_CLIENT_SUCCESS: 'REGISTER_CLIENT_SUCCESS',
  REGISTER_CLIENT_ERROR: 'REGISTER_CLIENT_ERROR',
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  SIGNUP_REQUEST: 'SIGNUP_REQUEST',
  LOGOUT: 'LOGOUT',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_ERROR: 'LOGIN_ERROR',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  registerClient: () => ({
    type: actions.REGISTER_CLIENT,
  }),
  registerClientSuccess: (client_id, secret) => ({
    type: actions.REGISTER_CLIENT_SUCCESS,
    client_id: client_id,
    secret: secret,
  }),
  registerClientError: () => ({
    type: actions.REGISTER_CLIENT_ERROR,
  }),
  login: (payload) => ({
    type: actions.LOGIN_REQUEST,
    payload,
  }),
  signup: (payload) => ({
    type: actions.SIGNUP_REQUEST,
    payload
  }),
  logout: () => ({
    type: actions.LOGOUT,
  }),
};
export default actions;
