import actions from "./actions";

const initState = { 
  idToken: null,
  client_id: '',
  secret: '',
};

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return {
        idToken: action.token
      };
    case actions.REGISTER_CLIENT:
      return {
        ...state
      };
    case actions.REGISTER_CLIENT_SUCCESS:
      return {
        ...state,
        client_id: action.client_id,
        secret: action.secret,
      };
    case actions.REGISTER_CLIENT_ERROR:
      return {
        ...state
      };
    case actions.LOGOUT:
      return initState;
    default:
      return state;
  }
}
