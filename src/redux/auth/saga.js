import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getToken, clearToken } from '../../helpers/utility';
import actions from './actions';
import notification from '../../components/notification';

const fakeApiCall = true; // auth0 or express JWT

const signupUserRequest = async (payload) =>
    await fetch('http://3.91.240.95/v1/user', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    }).then(res => res.json())
    .then(res => res)
    .catch(error => error)

export function* signup() {
  yield takeEvery(actions.SIGNUP_REQUEST, function*(data) {
    const { payload } = data;
    console.log("payload is: " + payload);
    const userSignupResult = yield call(signupUserRequest, payload);
    console.log(userSignupResult);

    //TODO: fix this:
    if (Object.entries(userSignupResult).length !== 0 && userSignupResult.success) {
      const payload_ = {
        userInfo: {
          username: payload.email,
          password: payload.password,
        }
      };
      console.log("userInfo is: " + JSON.stringify(payload_));
      yield put({
        type: actions.LOGIN_REQUEST,
        payload: payload_,
      });
    } else {
      notification('error', "Incorrect information entered. Please try again");
    }

  });
}

const loginUserRequest = async (payload) =>
    await fetch('http://3.91.240.95/v1/oauth/token', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    }).then(res => res.json())
    .then(res => res)
    .catch(error => error)

export function* loginRequest() {
  yield takeEvery('LOGIN_REQUEST', function*({ payload }) {
    const { userInfo } = payload;
    const flag = false;
    console.log(payload);
    const access_token = localStorage.getItem('id_token');
    if (access_token) {
      yield put({
        type: actions.LOGIN_SUCCESS,
        token: access_token,
        profile: 'Profile',
      });
    } else {
      console.log("token.userInfo.username is: " + userInfo.username);
      console.log("token.userInfo.password is: " + userInfo.password);
      const user = {
        grant_type: 'password',
        client_id: localStorage.getItem('client_id'),
        client_secret: localStorage.getItem('client_secret'),
        username: userInfo.username,
        password: userInfo.password
      };
      console.log("user is: " + JSON.stringify(user));
      const userLoginResult = yield call(loginUserRequest, user);
      console.log(userLoginResult);
      if (Object.entries(userLoginResult).length !== 0 && userLoginResult.access_token) {
        console.log("I'm in if");
        const access_token = userLoginResult.access_token;
        yield put({
          type: actions.LOGIN_SUCCESS,
          token: access_token,
          profile: 'Profile',
        });
      } else {
        console.log("I'm in else");
        yield put({ type: actions.LOGIN_ERROR });
      }
    }
  });
}

export function* loginSuccess() {
  yield takeEvery(actions.LOGIN_SUCCESS, function*(payload) {
    yield localStorage.setItem('id_token', payload.token);
    yield put(push('/dashboard'));
    notification('success', "Signed in!");
  });
}

export function* loginError() {
  yield takeEvery(actions.LOGIN_ERROR, function*() {
    console.log("loginError");
    notification('error', "Incorrect username or password");
  });
}

export function* logout() {
  yield takeEvery(actions.LOGOUT, function*() {
    console.log('fff')
    clearToken();
    yield put(push('/'));
  });
}
export function* checkAuthorization() {
  yield takeEvery(actions.CHECK_AUTHORIZATION, function*() {
    const token = getToken().get('idToken');
    if (token) {
      yield put({
        type: actions.LOGIN_SUCCESS,
        token,
        profile: 'Profile',
      });
    }
  });
}

const registerClientRequest = async (name) =>
    await fetch('http://3.91.240.95/v1/client', {
      method: 'POST',
      body: JSON.stringify(name),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    }).then(res => res.json())
    .then(res => res)
    .catch(error => error)

export function* registerClient() {
  try {
    const name = {
      name: 'mydevicesecret'
    };
    const clientRegisterResult = yield call(registerClientRequest, name);
    if (Object.entries(clientRegisterResult).length !== 0 && clientRegisterResult.success === true) {
        const client = clientRegisterResult.data.client;
        const secret = clientRegisterResult.data.secret;
        yield put({
          type: actions.REGISTER_CLIENT_SUCCESS,
          client_id: client,
          secret,
        });
    } else {
      console.log("I'm in else");
      yield put(actions.registerClientError());
    }

  } catch(error) {
      console.log(JSON.stringify(error));
      yield put(actions.registerClientError());
  }
}

export function* registerClientSuccess() {
  yield takeEvery(actions.REGISTER_CLIENT_SUCCESS, function*(payload) {
    yield window.localStorage.setItem('client_id', payload.client_id);
    yield window.localStorage.setItem('client_secret', payload.secret);
  });
}

export function* registerClientError() {
  console.log("registerClientError");
  notification('error', "There was an error. Please try again");
}

export default function* rootSaga() {
  yield all([
    fork(checkAuthorization),
    fork(loginRequest),
    fork(loginSuccess),
    fork(loginError),
    fork(logout),
    fork(registerClient),
    fork(registerClientSuccess),
    fork(registerClientError),
    fork(signup),
  ]);
}
