const actions = {
  SET_PATIENTS: 'SET_PATIENTS',
  VIEW_PATIENTS: 'VIEW_PATIENTS',
  ADD_PATIENTS: 'ADD_PATIENTS',
  ADD_SUCCESS: 'ADD_SUCCESS',
  ADD_ERROR: 'ADD_ERROR',
  DELETE_PATIENTS: 'DELETE_PATIENTS',
  DETAIL_PATIENTS: 'DETAIL_PATIENTS',
  SET_DETAIL_PATIENTS: 'SET_DETAIL_PATIENTS',
  EDIT_PATIENTS: 'EDIT_PATIENTS',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  viewPatients: () => ({
    type: actions.VIEW_PATIENTS,
  }),
  addPatients: (payload) => ({
    type: actions.ADD_PATIENTS,
    payload
  }),
  addPatientsSuccess: (client_id, secret) => ({
    type: actions.ADD_SUCCESS,
    client_id: client_id,
    secret: secret,
  }),
  addError: () => ({
    type: actions.ADD_ERROR,
  }),
  deletePatient: (payload) => ({
    type: actions.DELETE_PATIENTS,
    payload
  }),
  detailPatients: (payload) => ({
    type: actions.DETAIL_PATIENTS,
    payload
  }),
  editPatients: (payload,id) => ({
    type: actions.EDIT_PATIENTS,
    payload,
    id
  }),
};
export default actions;
