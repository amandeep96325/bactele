import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getToken, clearToken } from '../../helpers/utility';
import actions from './actions';
import notification from '../../components/notification';

const fakeApiCall = true; // auth0 or express JWT
const url ="http://3.91.240.95/v1/";
const viewPatient = async () =>{
    return await fetch(url+'patients', {
      method: 'GET',
    //  body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }

export function* viewPatients() {
  yield takeEvery(actions.VIEW_PATIENTS, function*(data) {

    const patients = yield call(viewPatient);

    yield put({
      type: actions.SET_PATIENTS,
      payload: patients.data
    })
  });
}

const detailPatient = async (payload) =>{
  // console.log(payload);
    return await fetch(url+'patient/profile/'+payload, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }

export function* detailPatients() {
  yield takeEvery(actions.DETAIL_PATIENTS, function*(data) {
    const { payload } = data;
    const patients = yield call(detailPatient, payload);
    // console.log(patients);
    yield put({
      type: actions.SET_DETAIL_PATIENTS,
      payload: patients.data
    })
  });
}

const addPatient = async (payload) =>{
    return await fetch(url+'patient', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* addPatients() {
  yield takeEvery(actions.ADD_PATIENTS, function*(data) {
    const { payload } = data;

    console.log("payload is: " + payload);
    const patients = yield call(addPatient, payload);
    console.log(patients);

    //TODO: fix this:
    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield put({
        type: actions.ADD_SUCCESS,
        payload: patients.msg,
      });
    } else {
      yield put({ type: actions.ADD_ERROR });
    }

  });
}

export function* addPatientsSuccess() {
  yield takeEvery(actions.ADD_SUCCESS, function*(payload) {
    yield put(push('/dashboard/view_patients'));
    notification('success', payload.payload);
  });
}

export function* addError() {
  yield takeEvery(actions.ADD_ERROR, function*() {
    console.log("addError");
    notification('error', "Something went wrong");
  });
}


const delPatient = async (payload) =>{
  // console.log(payload);
    return await fetch(url+'patient/'+payload, {
      method: 'DELETE',
      // body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* deletePatient() {
  // debugger;
  yield takeEvery(actions.DELETE_PATIENTS, function*(data) {
    const { payload } = data;

    // console.log("payload is: " + payload);
    const patients = yield call(delPatient, payload);
    console.log(patients);

    //TODO: fix this:
    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield put({
        type: actions.ADD_SUCCESS,
        payload: patients.msg,
      });
    } else {
      yield put({ type: actions.ADD_ERROR });
    }

  });
}


export function* checkAuthorization() {
  yield takeEvery(actions.CHECK_AUTHORIZATION, function*() {
    const token = getToken().get('idToken');
    if (token) {
      yield put({
        type: actions.LOGIN_SUCCESS,
        token,
        profile: 'Profile',
      });
    }
  });
}


const editPatient = async (payload) =>{
    return await fetch(url+'patient/profile/'+payload.patientid, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* editPatients() {
  yield takeEvery(actions.EDIT_PATIENTS, function*(data) {
    const { payload } = data;

    const patients = yield call(editPatient, payload);
    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield put({
        type: actions.ADD_SUCCESS,
        payload: patients.msg,
      });
    } else {
      yield put({ type: actions.ADD_ERROR });
    }

  });
}


export default function* rootSaga() {

  yield all([
    fork(viewPatients),
    fork(addPatients),
    fork(addPatientsSuccess),
    fork(addError),
    fork(deletePatient),
    fork(detailPatients),
    fork(editPatients),
  ]);
}
