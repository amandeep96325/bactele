import actions from "./actions";

const initState = {
  id: 1,
  data: []
};

export default function authReducer(state = initState, action) {
  switch (action.type) {

    case actions.SET_PATIENTS:
      return {
        ...state,
        data: action.payload
      }
    case actions.ADD_PATIENTS_SUCCESS:
      return {
        ...state,
        client_id: action.client_id,
        secret: action.secret,
      };
    case actions.ADD_PATIENTS_ERROR:
      return {
        ...state
      };
    case actions.DELETE_PATIENTS:
      return {
        ...state
      };
    case actions.SET_DETAIL_PATIENTS:
      return {
        ...state,
        data: action.payload
      };
      case actions.EDIT_PATIENTS:
        return {
          ...state
        };
    // case actions.LOGOUT:
    //   return initState;
    default:
      return state;
  }
}
