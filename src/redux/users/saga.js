import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getToken, clearToken } from '../../helpers/utility';
import actions from './actions';
import notification from '../../components/notification';

const fakeApiCall = true; // auth0 or express JWT
const url ="http://3.91.240.95/v1/";
const viewUser = async () =>{
    return await fetch(url+'users', {
      method: 'GET',
    //  body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }

export function* viewUsers() {
  yield takeEvery(actions.VIEW_USERS, function*(data) {

    const users = yield call(viewUser);
    yield put({
      type: actions.SET_USERS,
      payload: users.data
    })
  });
}
const delUser = async (payload) =>{
  // console.log(payload);
    return await fetch(url+'user/'+payload, {
      method: 'DELETE',
      // body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* deleteUser() {
  // debugger;
  yield takeEvery(actions.DELETE_USERS, function*(data) {
    const { payload } = data;

    // console.log("payload is: " + payload);
    const patients = yield call(delUser, payload);
    console.log(patients);

    //TODO: fix this:
    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield put({
        type: actions.ADD_SUCCESS,
        payload: patients.msg,
      });
    } else {
      yield put({ type: actions.ADD_ERROR });
    }

  });
}

export function* addSuccess() {
  yield takeEvery(actions.ADD_SUCCESS, function*(payload) {
    yield put(push('/dashboard/view_users'));
    notification('success', payload.payload);
  });
}

export function* addError() {
  yield takeEvery(actions.ADD_ERROR, function*() {
    console.log("addError");
    notification('error', "Something went wrong");
  });
}

export default function* rootSaga() {

  yield all([
    fork(viewUsers),
    fork(addSuccess),
    fork(addError),
    fork(deleteUser),
  ]);
}
