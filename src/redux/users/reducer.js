import actions from "./actions";

const initState = {
  id: 1,
  data: []
};

export default function authReducer(state = initState, action) {
  switch (action.type) {

    case actions.SET_USERS:
      return {
        ...state,
        data: action.payload
      }
    case actions.DELETE_USERS:
      return {
        ...state
      };
    default:
      return state;
  }
}
