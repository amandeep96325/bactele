const actions = {
  SET_USERS: 'SET_USERS',
  VIEW_USERS: 'VIEW_USERS',
  // ADD_USERS: 'ADD_USERS',
  ADD_SUCCESS: 'ADD_SUCCESS',
  ADD_ERROR: 'ADD_ERROR',
  DELETE_USERS: 'DELETE_USERS',
  // DETAIL_USERS: 'DETAIL_USERS',
  // SET_DETAIL_USERS: 'SET_DETAIL_USERS',
  // EDIT_USERS: 'EDIT_USERS',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  viewUsers: () => ({
    type: actions.VIEW_USERS,
  }),
  deleteUser: (payload) => ({
    type: actions.DELETE_USERS,
    payload
  }),
  addSuccess: (client_id, secret) => ({
    type: actions.ADD_SUCCESS,
    client_id: client_id,
    secret: secret,
  }),
  addError: () => ({
    type: actions.ADD_ERROR,
  }),
};
export default actions;
