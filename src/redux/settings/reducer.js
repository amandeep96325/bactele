import actions from "./actions";

const initState = {
  id: 1,
  data: []
};

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case actions.SET_PROFILE:
      return {
        ...state,
        data: action.payload
      };
      case actions.EDIT_PROFILE:
        return {
          ...state
        };
      case actions.CHANGE_PASSWORD:
        return {
          ...state
        };
        case actions.ADD_SUCCESS:
          return {
            ...state,
            client_id: action.client_id,
            secret: action.secret,
          };
        case actions.ADD_ERROR:
          return {
            ...state
          };
    default:
      return state;
  }
}
