import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getToken, clearToken } from '../../helpers/utility';
import actions from './actions';
import notification from '../../components/notification';

const fakeApiCall = true; // auth0 or express JWT
const url ="http://3.91.240.95/v1/";
const viewProfiles = async () =>{
    return await fetch(url+'user/profile', {
      method: 'GET',
    //  body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }

export function* viewProfile() {
  yield takeEvery(actions.VIEW_PROFILE, function*(data) {

    const users = yield call(viewProfiles);
    yield put({
      type: actions.SET_PROFILE,
      payload: users.data
    })
  });
}

const editProfiles = async (payload) =>{
    return await fetch(url+'user/profile/', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* editProfile() {
  yield takeEvery(actions.EDIT_PROFILE, function*(data) {
    const { payload } = data;

    const patients = yield call(editProfiles, payload);
    // debugger;

    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield put({
        type: actions.ADD_SUCCESS,
        payload: patients.msg,
      });
    } else {
      yield put({ type: actions.ADD_ERROR });
    }

  });
}
export function* addSuccess() {
  yield takeEvery(actions.ADD_SUCCESS, function*(payload) {
    yield put(push('/dashboard/profile'));
    notification('success', payload.payload);
  });
}

export function* addError() {
  yield takeEvery(actions.ADD_ERROR, function*(payload) {
    console.log("addError");
    notification('error', payload.payload);
  });
}

const changePass = async (payload) =>{
    return await fetch(url+'user/profile/password', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* changePassword() {
  yield takeEvery(actions.CHANGE_PASSWORD, function*(data) {
    const { payload } = data;

    const patients = yield call(changePass, payload);
    // debugger;

    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield put({
        type: actions.ADD_SUCCESS,
        payload: patients.msg,
      });
    } else {
      yield put({ type: actions.ADD_ERROR, payload: patients.msg, });
    }

  });
}


export default function* rootSaga() {

  yield all([
    fork(viewProfile),
    fork(editProfile),
    fork(addSuccess),
    fork(addError),
    fork(changePassword),
  ]);
}
