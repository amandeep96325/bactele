const actions = {
  SET_PROFILE: 'SET_PROFILE',
  VIEW_PROFILE: 'VIEW_PROFILE',
  EDIT_PROFILE: 'EDIT_PROFILE',
  ADD_SUCCESS: 'ADD_SUCCESS',
  ADD_ERROR: 'ADD_ERROR',
  CHANGE_PASSWORD: 'CHANGE_PASSWORD',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  viewProfile: () => ({
    type: actions.VIEW_PROFILE,
  }),
  editProfile: (payload) => ({
    type: actions.EDIT_PROFILE,
    payload
  }),
  changePassword: (payload) => ({
    type: actions.CHANGE_PASSWORD,
    payload
  }),
  addSuccess: (client_id, secret) => ({
    type: actions.ADD_SUCCESS,
    client_id: client_id,
    secret: secret,
  }),
  addError: () => ({
    type: actions.ADD_ERROR,
  }),
};
export default actions;
