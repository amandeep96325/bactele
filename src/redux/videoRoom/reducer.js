import actions from "./actions";

const initState = {
  id: 1,
  data: []
};

export default function authReducer(state = initState, action) {
  switch (action.type) {

    case actions.ADD_ROOM_SUCCESS:
      return {
        ...state,
        client_id: action.client_id,
        secret: action.secret,
      };
    case actions.ADD_ROOM_ERROR:
      return {
        ...state
      };
    case actions.GET_TWILIO_TOKEN:
      return {
        ...state
      };
    case actions.JOIN_ROOM:
      return {
        ...state,
        data: action.payload
      };
    default:
      return state;
  }
}
