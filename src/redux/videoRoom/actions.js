const actions = {
  // SET_ROOMS: 'SET_ROOMS',
  JOIN_ROOM: 'JOIN_ROOM',
  ADD_ROOMS: 'ADD_ROOMS',
  ADD_ROOM_SUCCESS: 'ADD_ROOM_SUCCESS',
  ADD_ROOM_ERROR: 'ADD_ROOM_ERROR',
  GET_TWILIO_TOKEN: 'GET_TWILIO_TOKEN',
  // SET_DETAIL_USERS: 'SET_DETAIL_USERS',
  // EDIT_USERS: 'EDIT_USERS',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  // viewRooms: () => ({
  //   type: actions.VIEW_ROOMS,
  // }),
  addRooms: (payload) => ({
    type: actions.ADD_ROOMS,
    payload
  }),
  addRoomSuccess: (client_id, secret) => ({
    type: actions.ADD_ROOM_SUCCESS,
    client_id: client_id,
    secret: secret,
  }),
  addRoomError: () => ({
    type: actions.ADD_ROOM_ERROR,
  }),
  joinRoom: (payload) => ({
    type: actions.JOIN_ROOM,
    payload
  }),
};
export default actions;
