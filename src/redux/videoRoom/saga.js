import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getToken, clearToken } from '../../helpers/utility';
import actions from './actions';
import notification from '../../components/notification';
import Video from 'twilio-video';

const fakeApiCall = true; // auth0 or express JWT
const url ="http://3.91.240.95/v1/";

const addRoom = async (payload) =>{
    return await fetch(url+'video/room', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': `Bearer ${localStorage.id_token}`
      },
    }).then(res =>  res.json())
    .then(res => res)
    .catch(error => console.log('response_error:', error))
  }



export function* addRooms() {
  yield takeEvery(actions.ADD_ROOMS, function*(data) {
    const { payload } = data;

    console.log("payload is: " + payload);
    const patients = yield call(addRoom, payload);
    if (Object.entries(patients).length !== 0 && patients.success) {
      console.log("userInfo is: " + JSON.stringify(payload));
      yield window.localStorage.setItem('room_identifier', patients.data.identifier);
      yield put({
        type: actions.GET_TWILIO_TOKEN,
        payload: {'identifier':patients.data.identifier,'name':payload.name},
      });
    } else {
      yield put({ type: actions.ADD_ROOM_ERROR });
    }

  });
}

const getTokens = async (payload) =>{

    return await fetch('http://127.0.0.1:8000/token'/*+'twilio/access/token'*/, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      },
    }).then(res =>  res.text())
    .then(json => json)
    .catch(error => console.log('response_error:', error))


  }


export function* getTwilioToken() {
  yield takeEvery(actions.GET_TWILIO_TOKEN, function*(data) {
    const { payload } = data;

    const patients = yield call(getTokens, payload);
    
    if (patients!='') {
        yield put(push('/dashboard/join_room/'+patients));
    } else {
      yield put({ type: actions.ADD_ROOM_ERROR });
    }

  });
}




export function* addRoomSuccess() {
  yield takeEvery(actions.ADD_ROOM_SUCCESS, function*(payload) {
    yield put(push('/dashboard/add_room'));
    notification('success', payload.payload);
  });
}

export function* addRoomError() {
  yield takeEvery(actions.ADD_ROOM_ERROR, function*() {
    console.log("addError");
    notification('error', "Something went wrong");
  });
}


export function* checkAuthorization() {
  yield takeEvery(actions.CHECK_AUTHORIZATION, function*() {
    const token = getToken().get('idToken');
    if (token) {
      yield put({
        type: actions.LOGIN_SUCCESS,
        token,
        profile: 'Profile',
      });
    }
  });
}


export default function* rootSaga() {

  yield all([
    fork(addRooms),
    fork(addRoomSuccess),
    fork(addRoomError),
    fork(getTwilioToken),
  ]);
}
