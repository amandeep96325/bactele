import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Input from '../../components/uielements/input';
// import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import appActions from '../../redux/app/actions';
// import Auth0 from '../../helpers/auth0/index';
// import Firebase from '../../helpers/firebase';
// import FirebaseLogin from '../../components/firebase';
import IntlMessages from '../../components/utility/intlMessages';
import SignUpStyleWrapper from './signup.style';

const { login, signup } = authAction;
const { clearMenu } = appActions;

class SignUp extends Component {
  state = {
    redirectToReferrer: false,
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  // handleLogin = (token = false) => {
  //   const { login, clearMenu } = this.props;
  //   console.log(token, 'handlelogin');
  //   if (token) {
  //     login(token);
  //   } else {
  //     login();
  //   }
  //   clearMenu();
  //   this.props.history.push('/dashboard');
  // };

  handleSignup = () => {
    const { signup } = this.props;
    const userInfo = {
      first_name: document.getElementById('inputFirstName').value || '',
      last_name: document.getElementById('inputLastName').value || '',
      email: document.getElementById('inputEmail').value || '',
      password: document.getElementById('inputPassword').value || '',
      role: 'admin'
    };
    console.log("userInfo: " + userInfo);
    signup(userInfo);
  }

  render() {
    // const { history } = this.props;
    return (
      <SignUpStyleWrapper className="isoSignUpPage">
        <div className="isoSignUpContentWrapper">
          <div className="isoSignUpContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signUpTitle" />
              </Link>
            </div>

            <div className="isoSignUpForm">
              <div className="isoInputWrapper isoLeftRightComponent">
                <Input id="inputFirstName" size="large" placeholder="First name" />
                <Input id="inputLastName" size="large" placeholder="Last name" />
              </div>

              {/* <div className="isoInputWrapper">
                <Input size="large" placeholder="Username" />
              </div> */}

              <div className="isoInputWrapper">
                <Input id="inputEmail" size="large" placeholder="Email" />
              </div>

              <div className="isoInputWrapper">
                <Input id="inputPassword" size="large" type="password" placeholder="Password" />
              </div>

              <div className="isoInputWrapper">
                <Input
                  id="inputConfirmPassword"
                  size="large"
                  type="password"
                  placeholder="Confirm Password"
                />
              </div>

              {/* Commenting out terms and conditions checkbox */}
              <div className="isoInputWrapper" style={{ marginBottom: '40px' }}>
                {/* <Checkbox>
                  <IntlMessages id="page.signUpTermsConditions" />
                </Checkbox> */}
              </div>

              <div className="isoInputWrapper">
                <Button type="primary" onClick={this.handleSignup}>
                  <IntlMessages id="page.signUpButton" />
                </Button>
              </div>

              {/* Commenting out other signup buttons */}
              {/* <div className="isoInputWrapper isoOtherLogin">
                <Button
                  onClick={this.handleLogin}
                  type="primary"
                  className="btnFacebook"
                >
                  <IntlMessages id="page.signUpFacebook" />
                </Button>
                <Button
                  onClick={this.handleLogin}
                  type="primary"
                  className="btnGooglePlus"
                >
                  <IntlMessages id="page.signUpGooglePlus" />
                </Button>
                {Auth0.isValid && (
                  <Button
                    onClick={() => {
                      Auth0.login();
                    }}
                    type="primary"
                    className="btnAuthZero"
                  >
                    <IntlMessages id="page.signUpAuth0" />
                  </Button>
                )}

                {Firebase.isValid && (
                  <FirebaseLogin
                    signup={true}
                    histor={history}
                    login={this.props.login}
                  />
                )}
              </div> */}

              <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
                <Link to="/signin">
                  <IntlMessages id="page.signUpAlreadyAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignUpStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
  }),
  { login, clearMenu, signup }
)(SignUp);
