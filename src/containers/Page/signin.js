import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Input from '../../components/uielements/input';
import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import appAction from '../../redux/app/actions';
// import Auth0 from '../../helpers/auth0';
// import Firebase from '../../helpers/firebase';
// import FirebaseLogin from '../../components/firebase';
import IntlMessages from '../../components/utility/intlMessages';
import SignInStyleWrapper from './signin.style';

const { login, registerClient } = authAction;
const { clearMenu } = appAction;

class SignIn extends Component {
  state = {
    redirectToReferrer: false,
  };

  registerClient = () => {
    // console.log("I'm in registerClient");
    this.props.registerClient();
  }

  componentWillMount() {
    //Checking to see if client already has client_id and secret
    if (!window.localStorage.getItem('client_id') || !window.localStorage.getItem('secret')) {
      this.registerClient();
    }
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  handleLogin = (token = false) => {
    const { login, clearMenu, history } = this.props;
    const userInfo = {
      username: document.getElementById('inputUserName').value || '',
      password: document.getElementById('inpuPassword').value || '',
    };
    if (token) {
      login({token, userInfo, history});
    } else {
      login();
    }
    clearMenu();
    this.props.history.push('/dashboard');
  };
  render() {
    // const { history } = this.props;
    const from = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <SignInStyleWrapper className="isoSignInPage">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signInTitle" />
              </Link>
            </div>

            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Input id="inputUserName" size="large" placeholder="Username" />
              </div>

              <div className="isoInputWrapper">
                <Input id="inpuPassword" size="large" type="password" placeholder="Password" />
              </div>

              <div className="isoInputWrapper isoLeftRightComponent">
                <Checkbox>
                  <IntlMessages id="page.signInRememberMe" />
                </Checkbox>
                <Button type="primary" onClick={this.handleLogin}>
                  <IntlMessages id="page.signInButton" />
                </Button>
              </div>

              {/* Commenting out helper message for sign in */}
              {/* <p className="isoHelperText">
                <IntlMessages id="page.signInPreview" />
              </p> */}

              {/* Commenting out other login buttons */}
              {/* <div className="isoInputWrapper isoOtherLogin">
                <Button
                  onClick={this.handleLogin}
                  type="primary"
                  className="btnFacebook"
                >
                  <IntlMessages id="page.signInFacebook" />
                </Button>
                <Button
                  onClick={this.handleLogin}
                  type="primary"
                  className="btnGooglePlus"
                >
                  <IntlMessages id="page.signInGooglePlus" />
                </Button>

                {Auth0.isValid && (
                  <Button
                    onClick={() => {
                      Auth0.login();
                    }}
                    type="primary"
                    className="btnAuthZero"
                  >
                    <IntlMessages id="page.signInAuth0" />
                  </Button>
                )}

                {Firebase.isValid && (
                  <FirebaseLogin history={history} login={this.props.login} />
                )}
              </div> */}

              <div className="isoCenterComponent isoHelperWrapper">
                {/* Commenting out signup and forget password link */}
                {/*
                <Link to="/forgotpassword" className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPass" />
                </Link>
                 <Link to="/signup">
                  <IntlMessages id="page.signInCreateAccount" />
                </Link>  */}
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
  }),
  { login, clearMenu, registerClient }
)(SignIn);
